# Messenger-Customer-Chat-for-WordPress
Display Messenger chatbox on your website, help your customers easy to contact with your business.

DEMO

https://doopchat.com

DESCRIPTIONS

Display Messenger chatbox on your website, help your customers easy to contact with your business.

This is WordPress version, other platforms version will release soon. Follow us to get news: https://www.facebook.com/1622113811187434/

FEATURES


Help your clients easy contact with your business
Don’t miss potential clients
Grow your business
Get notification immediately
Increase your fan page like
Unlimited colors
Friendly
Be easy to use and customize
And more...


INSTALLATION
Manual installation is easy and takes fewer than one minute.

1. Download the plugin from wordpress.org, unpack it and upload the [Messenger Customer Chat] folder to your wp-content/plugins/ directory.
2. Activate the plugin through the ‘Plugins‘ menu in WordPress.
3. Go to your main WordPress menu > Messenger Chat and enter your fan page URL

You’re done. Enjoy.

CHANGLOG
= 1.0 =
- Version 1.0 Initial Release

SUPPORT

If you need any help, feel free to chat with me at https://m.me/1622113811187434/

Tommy

Founder @NinjaTeam
